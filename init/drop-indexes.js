const $mongo = require('../libs/mongo');

const collections = [
  $mongo.collection('posts'),
  $mongo.collection('users'),
  $mongo.collection('postDrafts'),
  $mongo.collection('userScript'),
  $mongo.collection('rates'),
  $mongo.collection('sessions'),
  $mongo.collection('contentIndex'),

  $mongo.collection('groupAccounts'),
  $mongo.collection('groupAccountsMembers'),
  $mongo.collection('trendingFilters'),
  $mongo.collection('groupAccountsInvites'),
  $mongo.collection('authorNamesSandbox'),
  $mongo.collection('contentCache'),
  $mongo.collection('contentBlacklist'),

  $mongo.collection('telegraphTokens'),
  $mongo.collection('postCovers'),
];

function errorFn (err) {
  console.log(err);
}

async function dropFn () {  
  await new Promise((resolve, reject) => {
    function _await () {
      if (typeof collections[0].collection !== 'undefined') {
        resolve();
      } else {
        setTimeout(_await, 500);
      }
    }

    _await();
  });

  for (let coll of collections) {
    await coll.collection.dropIndexes('*').catch(errorFn);
  }
  
}

console.log('Started!');
dropFn()
  .then(() => {
    console.log('Done');
    process.exit();
  })
  .catch((err) => {
    console.log(err);
    process.exit();
  });