const $mongo = require('../libs/mongo');

const contentBlacklistDB = $mongo.collection('contentBlacklist');
const blacklist = require('../reference/content-blacklist');

const promises = [];

function handleError (err) {
  console.log(err);
}

console.log('Start');

Object.keys(blacklist).forEach((type) => {
  const list = blacklist[type].forEach((value) => {
    const doc = {
      type,
      value
    };

    promises.push(contentBlacklistDB.insert(doc).then(() => {
      return value;
    }).catch(handleError));
  });
});

Promise.all(promises)
  .then((list) => {
    console.log('End');
    console.log('------------');
    console.log(list);
    process.exit();
  })
  .catch(handleError);