const mongo = require('../libs/mongo');

const trendingFiltersDB = mongo.collection('trendingFilters');

(async () => {
  const list = await trendingFiltersDB.find(false, {
    limit: 10000
  });

  for (let item of list) {
    const newLanguage = item.language === 'all' ? [] : [item.language];

    await trendingFiltersDB.editOne({
      _id: item._id
    }, {
      language: newLanguage
    });
  }

  console.log('edited %s docs', list.length);
})()
  .then(() => {
    process.exit();
  })
  .catch((err) => {
    console.log(err);
    process.exit();
  });
