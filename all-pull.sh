git checkout $TO
git pull origin $TO

cd ../dtp-api
git checkout $TO
git pull origin $TO
npm i 

cd ../dtp-frontend
git checkout $TO
git pull origin $TO
npm i 

cd ../dtp-publisher-bot
git checkout $TO
git pull origin $TO
npm i 

cd ../telegram-files
git checkout $TO
git pull origin $TO
npm i 

cd ../telegraph-aggregator
git checkout $TO
git pull origin $TO
npm i 

cd ../dtp-admin
git checkout $TO
git pull origin $TO
npm i 

cd ../cover-generator
git checkout $TO
git pull origin $TO
npm i 

# AFTER

cd ../dtp-frontend
gulp

pm2 restart all