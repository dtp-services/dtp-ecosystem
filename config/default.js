module.exports = {
  mongo: {
    db: 'DTP-Database',
    models: require('../../dtp-api/config/system/mongo-models')
  }
};